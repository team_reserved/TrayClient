#pragma once

#include "stdafx.h"

/* Standard C++ includes */
#include <stdlib.h>
#include <iostream>

/*
Include directly the different
headers from cppconn/ and mysql_driver.h + mysql_util.h
(and mysql_connection.h). This will reduce your build time!
*/
#include "mysql_connection.h"

//#include <cppconn/driver.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn\prepared_statement.h>

#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\include\\lib\\opt\\mysqlcppconn.dll")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\include\\lib\\opt\\mysqlcppconn.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\include\\lib\\opt\\mysqlcppconn-static.lib")

class RegisterSQL {
public:
	sql::Driver *driver = NULL;
	sql::Connection *connection = NULL;
	sql::Statement *statement = NULL;
	sql::PreparedStatement *preparedstatement = NULL;
	sql::ResultSet *resultset = NULL;
	sql::ResultSet *resultsetCheck = NULL;

	std::string masterUsername = "tray";
	std::string masterPassword = "";
	std::string host = "localhost";
	std::string port = "3306";
	std::string DBname = "phpmyadmin";
	std::string tableName = "users";

	std::string username = "";
	std::string password = "";

	bool ret = false;
	struct Failed {
		std::string reason = "default_reason";
		std::string errorCode = "default_errorcode";
	};
	Failed failedRegister;

	// constructor
	RegisterSQL(std::string pusername, std::string ppassword) :
		username(pusername), password(ppassword) {

	}

	// register
	void registerUser() {
		//cleanErrors();
		try {
			driver = get_driver_instance();
			connection = driver->connect("tcp://" + host + ":" + port + "", masterUsername, masterPassword);
			connection->setSchema(DBname);
					
			preparedstatement = connection->prepareStatement("INSERT INTO " + tableName + "(username, password) VALUES(?, ?)");
			preparedstatement->setString(1, username);
			preparedstatement->setString(2, password);
			preparedstatement->executeUpdate();

			delete resultset;
			delete resultsetCheck;
			delete statement;
			delete connection;
		}
		catch (sql::SQLException &e) {
			failedRegister.reason = e.what();
			failedRegister.errorCode = e.getErrorCode();
		}
	}

	// validate registration
	void validateRegister() {
		try {
			driver = get_driver_instance();
			connection = driver->connect("tcp://" + host + ":" + port + "", masterUsername, masterPassword);
			connection->setSchema(DBname);

			//statement = connection->createStatement();
			//resultset = statement->executeQuery("SELECT password FROM " + tableName + " WHERE username = '" + username + "'");

			preparedstatement = connection->prepareStatement("SELECT password FROM " + tableName + " WHERE username = ?");
			preparedstatement->setString(1, username);
			resultset = preparedstatement->executeQuery();

			while (resultset->next()) {
				if (password == resultset->getString(1)) {
					ret = true;
				}
			}

			delete resultset;
			delete statement;
			delete connection;
		}
		catch (sql::SQLException &e) {
			failedRegister.reason = e.what();
			failedRegister.errorCode = e.getErrorCode();
		}
	}

	std::string getBoolee() {
		int retu = ret;
		std::string retuu = std::to_string(retu);
		return retuu;
	}

	// clean errors
	void cleanErrors() {
		failedRegister.reason = "";
		failedRegister.errorCode = "";
	}

	// send back register data
	std::string returnRegisterData() {
		if (ret == true) {
			return "";
		}
		else {
			std::string tmp = failedRegister.reason + " : " + failedRegister.errorCode;
			if (failedRegister.errorCode == "") {
				tmp = "The error output is empty!";
			}
			return tmp;
		}
	}
};