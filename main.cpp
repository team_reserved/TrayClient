#include "UYIC\UYIC.h"

#include "serDataSRC.h"
#include "LoginSRC.h"
#include "RegisterSRC.h"

#include <KIR\KIR4_GDI_bitmap.h>
#define myTimer 1

using namespace std;

#pragma region Browser windows

// Browse local folder
string BrowseFolder(HWND phwnd) {
	BROWSEINFO bi = { 0 };
	bi.lpszTitle = ("Browse folder...");
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	bi.hwndOwner = phwnd;
	//memset(&bi, 0, sizeof(bi));

	OleInitialize(NULL);

	LPITEMIDLIST pIDL = SHBrowseForFolder(&bi);

	if (pIDL == NULL)
		return "";

	TCHAR *buffer = new TCHAR[MAX_PATH];
	if (!SHGetPathFromIDList(pIDL, buffer) != 0)
	{
		CoTaskMemFree(pIDL);
		return "";
	}

	CoTaskMemFree(pIDL);
	OleUninitialize();

	return buffer;
}
// END

// new try ifiledialog
string BrowseLocalFolder(HWND phwnd) {
	CoInitialize(NULL);
	IFileDialog *pfd;
	wchar_t *g_path = L"";
	if (SUCCEEDED(CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pfd)))) {
		DWORD dwOptions;
		if (SUCCEEDED(pfd->GetOptions(&dwOptions))) {
			pfd->SetOptions(dwOptions | FOS_PICKFOLDERS);
		}
		if (SUCCEEDED(pfd->Show(phwnd))) {
			IShellItem *psi;
			if (SUCCEEDED(pfd->GetResult(&psi))) {
				if (!SUCCEEDED(psi->GetDisplayName(SIGDN_DESKTOPABSOLUTEPARSING, &g_path))) {
					MessageBox(NULL, "GetIDListName() failed", NULL, NULL);
				}
				psi->Release();
			}
			//CoTaskMemFree(psi);
		}
		pfd->Release();
	}
	wstring tmp(g_path);
	string ret(tmp.begin(), tmp.end());
	CoUninitialize();
	return ret;
}
//
#pragma endregion

#pragma region Variables, functions

//void KIR_fwriteb(const string &path, const string &data) {
//	ofstream
//		file;
//	file.open(path, std::ios_base::app | std::ios_base::out);
//	file << data;
//	file.close();
//}
void startup(char *path) {
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	if (!CreateProcess(path, NULL, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
		return;

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}
static TCHAR szClass[] = TEXT("UYI"), szCaption[] = TEXT("UYI Tray - Settings");
static TCHAR szClassLoginWindow[] = TEXT("Login"), szCaptionLogin[] = TEXT("Login");
static TCHAR szClassRegisterWindow[] = TEXT("Register"), szCaptionRegister[] = TEXT("Register");
static const int TRAYICON_ID = 2410, WM_TRAYICONMSG = WM_USER + 2;

const char regLink[] = "http://localhost/reg.php";

#define ID_CAPTURE_IMAGE 1
#define ID_SETTINGS 2
#define ID_MY_IMAGES_L 3
#define ID_MY_IMAGES_O 4
#define ID_EXIT 5

string nameUploader = "UYI uploader.exe";
bool invalidPathLocal = true;
bool loggedin = false;

static __int32 ID = 0;

// my HWND
NOTIFYICONDATA nid = {};
HWND
	hwnd,						// main window
	lblLocalFolder,				// Label:		local folder
	txtLocalFolder,				// Textfield:	local folder
	btnLocalFolder,				// Button:		local folder
	btnLocalSaveChanges,		// Button:		local save changes

	btnLogin,					// Button:		show the login form window
	txtLoginUsername,			// Textfield:	login username
	txtLoginPassword,			// Textfield:	login password
	btnLoginLogin,				// Button:		button login
	btnLoginCancel,				// Button:		button cancel
	loginwindow,				// login window
	lblLoggedin,				// Label:		logged in as...

	btnRegister,				// Button:		show the register form
	txtRegisterUsername,		// Textfield:	register username
	txtRegisterPasswordFirst,	// Textfield:	register password
	txtRegisterPasswordSecond,	// Textfield:	register password check
	btnRegisterRegister,		// Button:		button register
	btnRegisterCancel,			// Button:		button cancel
	registerwindow;				// register window

// END

#pragma endregion

#pragma region Functions

#pragma region Check directory
///<summary>
/// Check if directory exists
///</summary>
///<param name="dirName_in">Name of the dir to check</param>
///<returns>Returns true is exists</returns>
bool dirExists(const std::string& dirName_in) {
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES) {
		return false;  //something is wrong with your path!
	}
	if (ftyp & FILE_ATTRIBUTE_DIRECTORY) {
		return true;   // this is a directory!
	}
	return false;    // this is not a directory!
}
#pragma endregion

// start uploader
void startUploader(int hotkeyID) {
	switch (hotkeyID) {
	case 0: {
		/*struct StartUploader::Flag flags("USERNAME'admin66", "SERVER'localhost:4356");
		list<StartUploader::Flag> listt;
		listt.push_front(flags);
		writeDebug(StartUploader::GenerateCommandList(0, 11, listt).c_str());
		string tmp = "UYI.exe " + StartUploader::GenerateCommandList(0, 11, listt);*/
		//system(tmp.c_str());
		

		if (serdata.IDKEY == "") {
			MessageBox(hwnd, "Please login first!", "ERROR", NULL);
			return;
		}

		General tmp;
		KIR4::basic_string<KIR4::str_16> tmpp;
		KIR4::basic_string<KIR4::str_16> _un = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.LoginUsername));
		KIR4::basic_string<KIR4::str_16> _lf = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.PathLocalFolder));
		//_un = L"admin66";
		tmpp += KIR4::basic_string<KIR4::str_16>(L"ID'") + ID++;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'FLAGS'75'USERNAME'");	// windowed + setlink 75
		tmpp += _un;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'SERVER'localhost:4356'DIR'");
		tmpp += _lf;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'");
		clog << L"Creating thread with ID" << ID << KIR4::eol;
		clog << L"other data: " << tmpp.str() << KIR4::eol;
		tmp.SetData(tmpp);

		startNewProcess(tmp);
		break;
	}
	case 1: {
		if (serdata.IDKEY == "") {
			MessageBox(hwnd, "Please login first!", "ERROR", NULL);
			return;
		}

		General tmp;
		KIR4::basic_string<KIR4::str_16> tmpp;
		KIR4::basic_string<KIR4::str_16> _un = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.LoginUsername));
		KIR4::basic_string<KIR4::str_16> _lf = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.PathLocalFolder));
		//_un = L"admin66";
		tmpp += KIR4::basic_string<KIR4::str_16>(L"ID'") + ID++;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'FLAGS'83'USERNAME'");	// fullscr + setlink
		tmpp += _un;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'SERVER'localhost:4356'DIR'");	// 4356
		tmpp += _lf;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'");
		clog << L"Creating thread with ID" << ID << KIR4::eol;
		clog << L"other data: " << tmpp.str() << KIR4::eol;
		tmp.SetData(tmpp);
		startNewProcess(tmp);
		break;
	}
	case 2: {
		if (serdata.IDKEY == "") {
			MessageBox(hwnd, "Please login first!", "ERROR", NULL);
			return;
		}

		General tmp;
		KIR4::basic_string<KIR4::str_16> tmpp;
		KIR4::basic_string<KIR4::str_16> _un = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.LoginUsername));
		KIR4::basic_string<KIR4::str_16> _lf = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.PathLocalFolder));
		//_un = L"admin66";
		tmpp += KIR4::basic_string<KIR4::str_16>(L"ID'") + ID++;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'FLAGS'99'USERNAME'");	// browse file + setlink
		tmpp += _un;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'SERVER'localhost:4356'DIR'");
		tmpp += _lf;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'");
		clog << L"Creating thread with ID" << ID << KIR4::eol;
		clog << L"other data: " << tmpp.str() << KIR4::eol;
		tmp.SetData(tmpp);
		startNewProcess(tmp);
		break;
	}
	case 3: {
		if (serdata.IDKEY == "") {
			MessageBox(hwnd, "Please login first!", "ERROR", NULL);
			return;
		}

		General tmp;
		KIR4::basic_string<KIR4::str_16> tmpp;
		KIR4::basic_string<KIR4::str_16> _un = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.LoginUsername));
		KIR4::basic_string<KIR4::str_16> _lf = KIR4::basic_string<KIR4::str_16>(KIR4::str_16::tostr(serdata.PathLocalFolder));
		//_un = L"admin66";
		tmpp += KIR4::basic_string<KIR4::str_16>(L"ID'") + ID++;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'FLAGS'71'USERNAME'");	// from clipboard + setlink
		tmpp += _un;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'SERVER'localhost:4356'DIR'");
		tmpp += _lf;
		tmpp += KIR4::basic_string<KIR4::str_16>(L"'");
		clog << L"Creating thread with ID" << ID << KIR4::eol;
		clog << L"other data: " << tmpp.str() << KIR4::eol;
		tmp.SetData(tmpp);
		startNewProcess(tmp);
		break;
	}
		break;
	default:
		break;
	}
}

// check network availibilty
bool networkAvailibilty() {
	return InternetCheckConnection("http://www.google.com", FLAG_ICC_FORCE_CONNECTION, NULL);
}

// validate paths at startup
void validatePaths() {
	invalidPathLocal = !dirExists(serdata.PathLocalFolder);
}

// what is the problem / get last error
void whatsTheProblem() {
	DWORD error = GetLastError();
	char buf[256];
	LPCSTR textt = NULL;
	wsprintf(buf, "%u", error);
	textt = buf;
	MessageBox(NULL, textt, "ERROR", NULL);
}

// get window text
string getWindowText(HWND phwnd) {
	TCHAR buff[2048];
	GetWindowText(phwnd, buff, 2048);
	string ret = buff;
	return ret;
}

string getWindowpwText(HWND phwnd) {
	TCHAR buff[2048];
	GetWindowText(phwnd, buff, 2048);
	string ret = buff;
	std::reverse(ret.begin(), ret.end());
	return ret;
}

// login procedure
void loginProc(bool _state) {
	if (_state == true) {
		std::string tmp = "Logged in as " + serdata.LoginUsername;
		SetWindowText(lblLoggedin, tmp.c_str());
		ShowWindow(lblLoggedin, true);
		SetWindowText(btnLogin, "Logout");
	}
	else {
		std::string tmp = "";
		SetWindowText(lblLoggedin, tmp.c_str());
		ShowWindow(lblLoggedin, false);
		SetWindowText(btnLogin, "Login");
		serdata.IDKEY = "";
		serdata.LoginUsername = "";
		serdata.LoginPassword = "";
		Serialize();
	}
}

#pragma endregion

#pragma region MainProc
LRESULT CALLBACK MainProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	static HMENU hIconMenu = CreatePopupMenu();
	
	//NOTIFYICONDATA nid = {};

	switch (msg)
	{
	case WM_TIMER:
	{
		//clog << L"TIMEREVENTMOTHAFFF" << KIR4::eol;
		if (wParam == myTimer) {
			auto it = myList.begin();
			if (it != myList.end()) {
				(*it)->bsd.ApplyUploadPermission();
			}
			//clog << L"IFFIFIFIFIFIFIFIFIFIIF" << KIR4::eol;
			checkThreadStates();
			//clog << L"doneee :) =)" << KIR4::eol;
		}
		return 0;
	}
	case WM_CREATE:
	{
		nid.cbSize = sizeof(NOTIFYICONDATA);
		nid.uID = TRAYICON_ID;
		nid.hWnd = hwnd;
		nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_INFO | NIF_TIP | NIF_SHOWTIP;
		nid.dwInfoFlags = NIIF_ERROR;
		nid.uCallbackMessage = WM_TRAYICONMSG;

		static const GUID myGUID =
		{ 0x23977b55, 0x10e0, 0x4041, { 0xb8, 0x62, 0xb1, 0x95, 0x41, 0x96, 0x36, 0x69 } };

		nid.guidItem = myGUID;

		strcpy_s(nid.szTip, "Upload Your Image");

		nid.hIcon = (HICON)LoadImage(NULL, "UYI tray.ico", IMAGE_ICON, 0, 0, LR_LOADFROMFILE);

		Shell_NotifyIcon(NIM_ADD, &nid);
		Shell_NotifyIcon(NIM_SETVERSION, &nid);

		//AppendMenu(hIconMenu, MF_STRING, ID_CAPTURE_IMAGE, TEXT("Capture image"));
		AppendMenu(hIconMenu, MF_STRING, ID_SETTINGS, TEXT("Settings"));
		AppendMenu(hIconMenu, MF_STRING, ID_MY_IMAGES_L, TEXT("My images (local)"));
		AppendMenu(hIconMenu, MF_STRING, ID_MY_IMAGES_O, TEXT("My images (online)"));
		AppendMenu(hIconMenu, MF_STRING, ID_EXIT, TEXT("Exit"));

		//hotkey
		RegisterHotKey(hwnd, 1, MOD_CONTROL | MOD_SHIFT | MOD_NOREPEAT, 0x42);	// CTRL + SHIFT + B
		RegisterHotKey(hwnd, 2, MOD_CONTROL | MOD_SHIFT | MOD_NOREPEAT, 0x48);	// CTRL + SHIFT + H
		RegisterHotKey(hwnd, 3, MOD_CONTROL | MOD_SHIFT | MOD_NOREPEAT, 0x50);	// CTRL + SHIFT + P
		RegisterHotKey(hwnd, 4, MOD_CONTROL | MOD_SHIFT | MOD_NOREPEAT, 0x4D);	// CTRL + SHIFT + M

		return 0;
	}
	case WM_TRAYICONMSG:
	{
		if (lParam == WM_RBUTTONDOWN)			// right click on icon
		{
			POINT p;
			GetCursorPos(&p);
			SetForegroundWindow(hwnd);
			TrackPopupMenu(hIconMenu, TPM_LEFTALIGN, p.x, p.y, 0, hwnd, 0);
		}
		else if (lParam == WM_LBUTTONDOWN)		// left click on icon
		{
			ShowWindow(hwnd, SW_SHOWNORMAL);
		}
		return 0;
	}
	case WM_HOTKEY:
	{
		if ((int)wParam == 1) {		// CTRL + SHIFT + B
			//NOTIFYICONDATA nid={};
			nid.cbSize = nid.cbSize = sizeof(NOTIFYICONDATA);
			nid.uID = TRAYICON_ID;
			nid.hWnd = hwnd;
			nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_INFO | NIF_TIP | NIF_SHOWTIP;
			nid.dwInfoFlags = NIIF_ERROR;
			nid.uCallbackMessage = WM_TRAYICONMSG;

			static const GUID myGUID =
			{ 0x23977b55, 0x10e0, 0x4041,{ 0xb8, 0x62, 0xb1, 0x95, 0x41, 0x96, 0x36, 0x69 } };

			nid.guidItem = myGUID;

			strcpy_s(nid.szTip, "Upload Your Image");

			nid.hIcon = (HICON)LoadImage(NULL, "UYI tray.ico", IMAGE_ICON, 0, 0, LR_LOADFROMFILE);

			strcpy_s(nid.szInfoTitle, "Hot key pressed!");
			strcpy_s(nid.szInfo, "Keycode:\nCtrl + Shift + B");
			//Shell_NotifyIcon(NIM_MODIFY, &nid);
			//Shell_NotifyIcon(NIM_SETVERSION, &nid);

			//ShowWindow(hwnd, SW_SHOWNORMAL);

			startUploader(0);
			return 0;
		}
		else if ((int)wParam == 2) {	// CTRL + SHIFT + H
			//NOTIFYICONDATA nid={};
			nid.cbSize = nid.cbSize = sizeof(NOTIFYICONDATA);
			nid.uID = TRAYICON_ID;
			nid.hWnd = hwnd;
			nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_INFO | NIF_TIP | NIF_SHOWTIP;
			nid.dwInfoFlags = NIIF_ERROR;
			nid.uCallbackMessage = WM_TRAYICONMSG;

			static const GUID myGUID =
			{ 0x23977b55, 0x10e0, 0x4041,{ 0xb8, 0x62, 0xb1, 0x95, 0x41, 0x96, 0x36, 0x69 } };

			nid.guidItem = myGUID;

			strcpy_s(nid.szTip, "Upload Your Image");

			nid.hIcon = (HICON)LoadImage(NULL, "UYI tray.ico", IMAGE_ICON, 0, 0, LR_LOADFROMFILE);

			strcpy_s(nid.szInfoTitle, "Hot key pressed!");
			strcpy_s(nid.szInfo, "Keycode:\nCtrl + Shift + H");
			//Shell_NotifyIcon(NIM_MODIFY, &nid);
			//Shell_NotifyIcon(NIM_SETVERSION, &nid);

			//ShowWindow(hwnd, SW_SHOWNORMAL);

			startUploader(1);
			return 0;
		}
		else if ((int)wParam == 3) {	// CTRL + SHIFT + P
			startUploader(2);
			return 0;
		}
		else if ((int)wParam == 4) {
			startUploader(3);
			return 0;
		}
		else {
			return 0;
		}
		
	}
	case WM_CTLCOLOREDIT:
	{
		if (((HWND)lParam == txtLocalFolder) & (invalidPathLocal == true)) {
			return (INT_PTR)CreateSolidBrush(RGB(255, 0, 0));
			// if edit control is in dialog procedure change LRESULT to INT_PTR
		}
		else if (((HWND)lParam == txtLocalFolder) & (invalidPathLocal == false)) {
			return (INT_PTR)CreateSolidBrush(RGB(255, 255, 255));
		}
		else {  // this is some other static control, do not touch it!!
			//MessageBox(hwnd, "nemlefut", "nemlefut", NULL);
			return DefWindowProc(hwnd, msg, wParam, lParam);
		}
	}
	case WM_COMMAND:
	{
		// Controls

		// Browse default local folder
		if (lParam == (LPARAM)btnLocalFolder && wParam == BN_CLICKED) {
			string str = BrowseLocalFolder(hwnd);
			if (str == "") {
				str = serdata.PathLocalFolder;
			}
			SetWindowText(txtLocalFolder, str.c_str());
			serdata.PathLocalFolder = str;
			Serialize();
		}
		// local
		if (lParam == (LPARAM)txtLocalFolder && HIWORD(wParam) == EN_CHANGE) {
			TCHAR buff[2048];
			GetWindowText(txtLocalFolder, buff, 2048);
			bool exists = dirExists(buff);
			EnableWindow(btnLocalSaveChanges, exists);
			if (exists == true) {
				invalidPathLocal = false;
			}
			else {
				invalidPathLocal = true;
			}
			InvalidateRect(txtLocalFolder, NULL, false);
		}
		// local save
		if (lParam == (LPARAM)btnLocalSaveChanges && wParam == BN_CLICKED) {
			TCHAR tmp[2048];
			GetWindowText(txtLocalFolder, tmp, 2048);
			serdata.PathLocalFolder = tmp;
			Serialize();
			EnableWindow(btnLocalSaveChanges, false);
		}

		// login
		if (lParam == (LPARAM)btnLogin && wParam == BN_CLICKED) {
			if (loggedin == false) {
				EnableWindow(hwnd, false);
				RECT hwndRect = { 0 };
				GetWindowRect(hwnd, &hwndRect);
				RECT currentRect = { 0 };
				GetWindowRect(loginwindow, &currentRect);
				SetWindowPos(loginwindow, hwnd, hwndRect.left, hwndRect.top, 340, 150, SWP_SHOWWINDOW);
				// clean the input fields
				SetWindowText(txtLoginUsername, "");
				SetWindowText(txtLoginPassword, "");
			}
			else {
				loggedin = false;
				loginProc(loggedin);
			}
		}

		// register
		if (lParam == (LPARAM)btnRegister && wParam == BN_CLICKED) {
			//EnableWindow(hwnd, false);
			//RECT hwndRect = { 0 };
			//GetWindowRect(hwnd, &hwndRect);
			//RECT currentRect = { 0 };
			//GetWindowRect(registerwindow, &currentRect);
			//SetWindowPos(registerwindow, hwnd, hwndRect.left, hwndRect.top, 340, 170, SWP_SHOWWINDOW);
			//// clean the input fields
			//SetWindowText(txtRegisterUsername, "");
			//SetWindowText(txtRegisterPasswordFirst, "");
			//SetWindowText(txtRegisterPasswordSecond, "");
			ShellExecute(NULL, "open", regLink, 0, 0, SW_SHOWNORMAL);
		}

		// END

		switch (LOWORD(wParam))
		{
		case ID_CAPTURE_IMAGE:
		{
			startup("UYI.exe");
			return 0;
		}
		case ID_SETTINGS:
		{
			ShowWindow(hwnd, SW_SHOWNORMAL);
			UpdateWindow(hwnd);
			return 0;
		}
		case ID_MY_IMAGES_L:
		{
			//ShellExecute(NULL, "open", "http://google.com", NULL, NULL, SW_SHOWNORMAL);
			ShellExecute(NULL, "open", serdata.PathLocalFolder.c_str(), NULL, NULL, SW_SHOWNORMAL);
			return 0;
		}
		case ID_MY_IMAGES_O:
		{
			ShellExecute(NULL, "open", "localhost", NULL, NULL, SW_SHOWNORMAL);
			return 0;
		}
		case ID_EXIT:
		{
			Shell_NotifyIcon(NIM_DELETE, &nid);
			//SendMessage(hwnd, WM_MOUSEMOVE, 0, 0);
			exit(0);
			return 0;
		}
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
		}
	}
	case WM_DESTROY:
	{
		NOTIFYICONDATA nid;
		nid.cbSize = sizeof(NOTIFYICONDATA);
		nid.hWnd = hwnd;
		nid.uID = TRAYICON_ID;
		Shell_NotifyIcon(NIM_DELETE, &nid);

		PostQuitMessage(0);
		_CrtDumpMemoryLeaks();

		// timer
		KillTimer(hwnd, myTimer);

		return 0;
	}
	case WM_CLOSE:
	{
		ShowWindow(hwnd, SW_HIDE);
		_CrtDumpMemoryLeaks();
		return 0;
	}
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
}
#pragma endregion

#pragma region LoginWindowsProc

LRESULT CALLBACK LoginWindowProc(HWND loginwindow, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
	case WM_COMMAND:
	{
		if (lParam == (LPARAM)btnLoginCancel && wParam == BN_CLICKED) {
			SetWindowText(txtLoginUsername, "");
			SetWindowText(txtLoginPassword, "");
			EnableWindow(hwnd, true);
			ShowWindow(loginwindow, false);
			SetFocus(hwnd);
		}
		if (lParam == (LPARAM)btnLoginLogin && wParam == BN_CLICKED) {
			if (networkAvailibilty() == false) {
				MessageBox(hwnd, "Could not conncet to the server.\nCheck your internet connection or try again later.", "ERROR", NULL);
				ShowWindow(loginwindow, false);
				EnableWindow(hwnd, true);
				SetFocus(hwnd);
				break;
			}
			// login, this is the SQL part of the story
			LoginSQL loginsql(getWindowText(txtLoginUsername), sha512(getWindowpwText(txtLoginPassword)));
			loginsql.LoginUser();
			if (loginsql.returnLoginData() == "") {
				MessageBox(NULL, "Success!", "Info", MB_ICONINFORMATION);
				EnableWindow(hwnd, true);
				ShowWindow(loginwindow, false);
				loggedin = true;
				loginProc(loggedin);
				SetFocus(hwnd);
			}
			else {
				MessageBox(NULL, loginsql.returnLoginData().c_str(), "Error", MB_ICONERROR);
			}
		}
	}
	case WM_DESTROY:
	{
		return 0;
	}
	case WM_CLOSE:
	{
		EnableWindow(hwnd, true);
		ShowWindow(loginwindow, SW_HIDE);
		return 0;
	}
	default:
		return DefWindowProc(loginwindow, msg, wParam, lParam);
	}
}

#pragma endregion

#pragma region RegisterWindowProc

LRESULT CALLBACK RegisterWindowProc(HWND registerwindow, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
	case WM_COMMAND:
	{
		if (lParam == (LPARAM)btnRegisterCancel && wParam == BN_CLICKED) {
			SetWindowText(txtRegisterUsername, "");
			SetWindowText(txtRegisterPasswordFirst, "");
			SetWindowText(txtRegisterPasswordSecond, "");
			EnableWindow(hwnd, true);
			ShowWindow(registerwindow, false);
		}
		if (lParam == (LPARAM)btnRegisterRegister && wParam == BN_CLICKED) {
			if (getWindowText(txtRegisterPasswordFirst) != getWindowText(txtRegisterPasswordSecond)) {
				MessageBox(NULL, "The passwords are not the same!", "ERROR", MB_ICONERROR);
				return 0;
			}
			RegisterSQL registersql(getWindowText(txtRegisterUsername), sha512(getWindowText(txtRegisterPasswordFirst)));
			registersql.registerUser();
			registersql.validateRegister();
			if (registersql.returnRegisterData() == "") {
				MessageBox(NULL, "Success!", "Info", MB_ICONINFORMATION);
				EnableWindow(hwnd, true);
				ShowWindow(registerwindow, false);
				SetFocus(hwnd);
			}
			else {
				if (registersql.returnRegisterData().substr(0, 9) == "Duplicate") {
					MessageBox(NULL, "This username is already taken!", "ERROR", MB_ICONERROR);
				}
				else {
					MessageBox(NULL, registersql.returnRegisterData().c_str(), "ERROR", MB_ICONERROR);
				}
			}
		}
	}
	case WM_DESTROY :
	{
		return 0;
	}
	case WM_CLOSE:
	{
		EnableWindow(hwnd, true);
		ShowWindow(registerwindow, false);
		return 0;
	}
	default:
		return DefWindowProc(registerwindow, msg, wParam, lParam);
	}
}

#pragma endregion

#pragma region WinMain
int WINAPI WinMain(HINSTANCE hThisInstance, HINSTANCE hPrevInstance, LPSTR lpszArgument, int nFunsterStil) {
//int main() {
	KIR4::gdi_bitmap::initialize();
	//HINSTANCE hThisInstance = NULL;
	writeDebug("STARTED");
	Deserialize();
	validatePaths();
	loggedin = isUserLoggedIn();

	MSG messages;

	WNDCLASSEX wincl = { 0 };

	wincl.hInstance = hThisInstance;
	wincl.lpszClassName = szClass;
	wincl.lpfnWndProc = MainProc;
	wincl.style = CS_DBLCLKS | CS_VREDRAW | CS_HREDRAW;
	wincl.cbSize = sizeof(WNDCLASSEX);
	wincl.hIcon = LoadIcon(NULL, "UYI tray.ico");
	wincl.hIconSm = (HICON)LoadImage(NULL, "UYI tray.ico", IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
	wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
	wincl.lpszMenuName = NULL;
	wincl.cbClsExtra = 0;
	wincl.cbWndExtra = 0;
	wincl.hbrBackground = GetSysColorBrush(COLOR_3DFACE);

	WNDCLASSEX winLoginWindow = { 0 };

	winLoginWindow.hInstance = hThisInstance;
	winLoginWindow.lpszClassName = szClassLoginWindow;
	winLoginWindow.lpfnWndProc = LoginWindowProc;
	winLoginWindow.style = CS_DBLCLKS | CS_VREDRAW | CS_HREDRAW;	// CS_DROPSHADOW
	winLoginWindow.cbSize = sizeof(WNDCLASSEX);
	winLoginWindow.hIcon = LoadIcon(NULL, "UYI tray.ico");
	winLoginWindow.hIconSm = (HICON)LoadImage(NULL, "UYI tray.ico", IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
	winLoginWindow.hCursor = LoadCursor(NULL, IDC_ARROW);
	winLoginWindow.lpszMenuName = NULL;
	winLoginWindow.cbClsExtra = 0;
	winLoginWindow.cbWndExtra = 0;
	winLoginWindow.hbrBackground = GetSysColorBrush(COLOR_3DFACE);

	WNDCLASSEX winRegisterWindow = { 0 };

	winRegisterWindow.hInstance = hThisInstance;
	winRegisterWindow.lpszClassName = szClassRegisterWindow;
	winRegisterWindow.lpfnWndProc = RegisterWindowProc;
	winRegisterWindow.style = CS_DBLCLKS | CS_VREDRAW | CS_HREDRAW;	// CS_DROPSHADOW
	winRegisterWindow.cbSize = sizeof(WNDCLASSEX);
	winRegisterWindow.hIcon = LoadIcon(NULL, "UYI tray.ico");
	winRegisterWindow.hIconSm = (HICON)LoadImage(NULL, "UYI tray.ico", IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
	winRegisterWindow.hCursor = LoadCursor(NULL, IDC_ARROW);
	winRegisterWindow.lpszMenuName = NULL;
	winRegisterWindow.cbClsExtra = 0;
	winRegisterWindow.cbWndExtra = 0;
	winRegisterWindow.hbrBackground = GetSysColorBrush(COLOR_3DFACE);

	if (!RegisterClassEx(&wincl)) {
		MessageBox(hwnd, "Could not create main window!", "ERROR", NULL);
		return 0;
	}
	if (!RegisterClassEx(&winLoginWindow)) {
		whatsTheProblem();
		return 0;
	}
	if (!RegisterClassEx(&winRegisterWindow)) {
		whatsTheProblem();
		return 0;
	}

	// create main window
	hwnd = CreateWindowEx(0, szClass, szCaption, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 600, 400, HWND_DESKTOP, NULL, hThisInstance, NULL);

	loginwindow = CreateWindowEx(0, szClassLoginWindow, szCaptionLogin, WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MINIMIZEBOX ^ WS_MAXIMIZEBOX, 20, 40, 340, 150, hwnd, NULL, hThisInstance, NULL);

	registerwindow = CreateWindowEx(0, szClassRegisterWindow, szCaptionRegister, WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MINIMIZEBOX ^ WS_MAXIMIZEBOX, 20, 40, 340, 170, hwnd, NULL, hThisInstance, NULL);

	// BROWSE local folder
	lblLocalFolder = CreateWindow("STATIC", "Browse folder", (WS_VISIBLE | WS_CHILD)
		, 10, 10, 140, 22, hwnd, NULL, hThisInstance, NULL);
	btnLocalFolder = CreateWindow("BUTTON", "�", (WS_VISIBLE | WS_CHILD | BS_CENTER)	// ALT+0133
		, 320, 35, 25, 22, hwnd, NULL, hThisInstance, NULL);
	txtLocalFolder = CreateWindow("EDIT", serdata.PathLocalFolder.c_str(), (WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL | WS_BORDER)
		, 10, 35, 300, 21, hwnd, NULL, hThisInstance, NULL);
	btnLocalSaveChanges = CreateWindow("BUTTON", "Save", (WS_VISIBLE | WS_CHILD | BS_CENTER | WS_DISABLED)	// make disable by default
		, 355, 35, 50, 22, hwnd, NULL, hThisInstance, NULL);

	// Login
	btnLogin = CreateWindow("BUTTON", "Login", (WS_VISIBLE | WS_CHILD)
		, 10, 75, 50, 24, hwnd, NULL, hThisInstance, NULL);

	txtLoginUsername = CreateWindow("EDIT", "", (WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL | WS_BORDER)
		, 10, 10, 300, 21, loginwindow, NULL, hThisInstance, NULL);
	txtLoginPassword = CreateWindow("EDIT", "", (WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL | WS_BORDER | ES_PASSWORD)
		, 10, 40, 300, 22, loginwindow, NULL, hThisInstance, NULL);
	btnLoginLogin = CreateWindow("BUTTON", "Login", (WS_VISIBLE | WS_CHILD | BS_CENTER)
		, 10, 70, 50, 24, loginwindow, NULL, hThisInstance, NULL);
	btnLoginCancel = CreateWindow("BUTTON", "Cancel", (WS_VISIBLE | WS_CHILD | BS_CENTER)
		, 70, 70, 50, 24, loginwindow, NULL, hThisInstance, NULL);
	lblLoggedin = CreateWindow("STATIC", "Logged in as ", (WS_VISIBLE | WS_CHILD)
		, 150, 75, 200, 22, hwnd, NULL, hThisInstance, NULL);
	ShowWindow(lblLoggedin, false);

	// Register
	btnRegister = CreateWindow("BUTTON", "Register", (WS_VISIBLE | WS_CHILD)
		, 70, 75, 60, 24, hwnd, NULL, hThisInstance, NULL);

	txtRegisterUsername = CreateWindow("EDIT", "", (WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL | WS_BORDER)
		, 10, 10, 300, 21, registerwindow, NULL, hThisInstance, NULL);
	txtRegisterPasswordFirst = CreateWindow("EDIT", "", (WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL | WS_BORDER | ES_PASSWORD)
		, 10, 40, 300, 22, registerwindow, NULL, hThisInstance, NULL);
	txtRegisterPasswordSecond = CreateWindow("EDIT", "", (WS_VISIBLE | WS_CHILD | ES_AUTOHSCROLL | WS_BORDER | ES_PASSWORD)
		, 10, 70, 300, 22, registerwindow, NULL, hThisInstance, NULL);
	btnRegisterRegister = CreateWindow("BUTTON", "Register", (WS_VISIBLE | WS_CHILD | BS_CENTER)
		, 10, 100, 60, 24, registerwindow, NULL, hThisInstance, NULL);
	btnRegisterCancel = CreateWindow("BUTTON", "Cancel", (WS_VISIBLE | WS_CHILD | BS_CENTER)
		, 80, 100, 50, 24, registerwindow, NULL, hThisInstance, NULL);

	// set font
	HFONT hFont = CreateFont(18, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, "Segoe UI");

	HWND hwndArray[] = { 
		hwnd,																															// default HWND
		lblLocalFolder, btnLocalSaveChanges, txtLocalFolder,																			// local folder HWND
		btnLogin, txtLoginUsername, txtLoginPassword, btnLoginLogin, btnLoginCancel, lblLoggedin,										// login HWND
		btnRegister, txtRegisterUsername, txtRegisterPasswordFirst, txtRegisterPasswordSecond, btnRegisterRegister, btnRegisterCancel	// register HWND
	};

	for (int i = 0; i < sizeof(hwndArray) / sizeof(*hwndArray); i++) {
		HWND *tmp = &hwndArray[i];
		SendMessage(*tmp, WM_SETFONT, WPARAM(hFont), TRUE);
	}

	//

	// Login proc

	loginProc(loggedin);

	//

	SetTimer(hwnd, myTimer, 1000 / 1, (TIMERPROC)NULL);

	while (GetMessage(&messages, NULL, 0, 0) > 0) {
		TranslateMessage(&messages);
		DispatchMessage(&messages);
	}

	KIR4::gdi_bitmap::shut_down();

	return messages.wParam;
}
#pragma endregion
