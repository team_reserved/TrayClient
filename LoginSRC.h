#pragma once

#include "stdafx.h"

/* Standard C++ includes */
#include <stdlib.h>
#include <iostream>

/*
Include directly the different
headers from cppconn/ and mysql_driver.h + mysql_util.h
(and mysql_connection.h). This will reduce your build time!
*/
#include "mysql_connection.h"

//#include <cppconn/driver.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn\prepared_statement.h>

#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\include\\lib\\opt\\mysqlcppconn.dll")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\include\\lib\\opt\\mysqlcppconn.lib")
#pragma comment(lib, "C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\include\\lib\\opt\\mysqlcppconn-static.lib")

class LoginSQL {
public:
	sql::Driver *driver = NULL;
	sql::Connection *connection = NULL;
	sql::Statement *statement = NULL;
	sql::ResultSet *resultset = NULL;
	sql::PreparedStatement *preparedstatement = NULL;
	
	std::string masterUsername = "tray";
	std::string masterPassword = "";	//VKrhJa3B8y8Vb8XG
	std::string host = "localhost";
	std::string port = "3306";
	std::string DBname = "users";
	std::string tableName = "data";

	std::string username = "";
	std::string password = "";
	std::string id = "";

	bool ret = false;
	struct Failed {
		std::string reason = "";
		std::string errorCode = "";
	};
	Failed failedLogin;

	// constructor
	LoginSQL(std::string pusername, std::string ppassword):
		username(pusername), password(ppassword) {
		
	}

	// login
	void LoginUser() {
		cleanErrors();
		try {
			driver = get_driver_instance();
			connection = driver->connect("tcp://" + host + ":" + port + "", masterUsername, masterPassword);
			connection->setSchema(DBname);

			//statement = connection->createStatement();
			//resultset = statement->executeQuery("SELECT password FROM " + tableName + " WHERE username = '" + username + "'");

			preparedstatement = connection->prepareStatement("SELECT password FROM " + tableName + " WHERE username = ?");
			preparedstatement->setString(1, username);
			resultset = preparedstatement->executeQuery();

			std::string tmp = "";
			while (resultset->next()) {
				tmp = resultset->getString(1);
			}
			//
			writeDebug(tmp.c_str());
			//
			if (tmp == password) {
				ret = true;
				preparedstatement = connection->prepareStatement("SELECT id FROM " + tableName + " WHERE username = ?");
				preparedstatement->setString(1, username);
				resultset = preparedstatement->executeQuery();
				while (resultset->next()) {
					id = resultset->getString(1);
				}
				serdata.LoginUsername = username;
				serdata.LoginPassword = password;
				serdata.IDKEY = id;
				Serialize();
			}

			delete resultset;
			delete statement;
			delete connection;
		}
		catch (sql::SQLException &e) {
			failedLogin.reason = e.what();
			failedLogin.errorCode = e.getErrorCode();
		}
	}

	std::string getBoolee() {
		int retu = ret;
		std::string retuu = std::to_string(retu);
		return retuu;
	}

	// clean errors
	void cleanErrors() {
		failedLogin.reason = "";
		failedLogin.errorCode = "";
	}

	// send back login data
	std::string returnLoginData() {
		if (ret == true) {
			return "";
		}
		else {
			std::string tmp = failedLogin.reason + " : " + failedLogin.errorCode;
			if (tmp == "") {
				tmp = "The error output is empty!";
			}
			return tmp;
		}
	}
};

// check if logged in
bool isUserLoggedIn() {
	if (serdata.LoginUsername == "") {
		return false;
	}
	else if (serdata.LoginPassword == "") {
		return false;
	}
	else if (serdata.IDKEY == "") {
		return false;
	}
	LoginSQL login(serdata.LoginUsername, serdata.LoginPassword);
	login.LoginUser();
	if (login.returnLoginData() == "") {
		return true;
	}
	else {
		return false;
	}
}