#pragma once

#include "StartUploader.h"

#include <windows.h>
#include <tchar.h>
#include <ShlObj.h>
#include <boost\uuid\sha1.hpp>
#include <WinInet.h>
#include <algorithm>
#pragma comment(lib, "wininet.lib")

//#include "KIR4_basic_string.h"
//#include "KIR4_console.h"
//#include "General.h"
#include "sha512m.h"
#include "DebugStream.h"
//#include "StartUploader.h"


#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <thread>

#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif