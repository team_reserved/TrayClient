#pragma once

#include <stdio.h>

FILE *file = NULL;
SYSTEMTIME st, lt;

void getTime() {
	GetSystemTime(&st);
	GetLocalTime(&lt);
}

void writeDebug(const char *dbmsg) {
	file = fopen("debug.txt", "a");
	if (file == NULL) {
		MessageBox(NULL, "FAILED TO OPEN DEBUG FILE", "ERROR", NULL);
	}
	// write time
	getTime();
	char time[128];
	sprintf(time, "\n____ [ENTRY %02d.%02d.%02d at %02d:%02d:%02d]____\n", lt.wYear, lt.wMonth, lt.wDay, lt.wHour, lt.wMinute, lt.wSecond);
	fprintf(file, time);
	// write message
	fprintf(file, dbmsg);
	fprintf(file, "\n__________________END__________________\n");
	fclose(file);
}