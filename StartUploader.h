#pragma once

#include <thread>
#include <list>
#include <KIR\KIR4_GDI_bitmap.h>

//void UYIC(General *general) {
//	clog << L"started thread with " << general->GetID() << KIR4::eol;
//	general->SetStatus(0);
//	while (general->GetStatus() < 100) {
//		Sleep(100);
//		general->SetStatus(general->GetStatus() + 1);
//	}
//	MessageBox(NULL, "anya", "apa", NULL);
//	clog << L"UYIC ended with " << general->GetID() << KIR4::eol;
//	general->SetDone(true);
//}

struct starter {
	General bsd;
	std::thread asd;
};

std::list<starter*> myList;

void startNewProcess(const General &_p) {
	starter *myStarter = new starter();
	/*if (myList.size() == NULL) {
		myStarter->bsd.ApplyUploadPermission();
	}
	else {
		myStarter->bsd.WaitForUploadPermission();
	}*/
	myStarter->bsd = _p;
	myStarter->bsd.SetDone(false);
	myStarter->asd = std::thread(UYIC, &(myStarter->bsd));
	myList.push_back(myStarter);
}

void checkThreadStates() {
	auto it = myList.begin();
	while (it != myList.end()) {
		clog << L"while" << KIR4::eol;
		//((*it)->asd.joinable()) == true
		if ((*it)->bsd.GetDone() == false) {
			clog << L"status in while: " << (*it)->bsd.GetStatus() << KIR4::eol;
		}
		else {
			if ((*it)->asd.joinable()) {
				
				(*it)->asd.join();
			}
			delete *it;
			*it = NULL;
			//clog << L"thread nulled out with ID" << (*it)->bsd.GetID() << KIR4::eol;
			//if (myList.size() == 0) {
			//	//KIR4::gdi_bitmap::shut_down();
			//}
		}
		it++;
	}
	it = myList.begin();
	while (it != myList.end()) {
		if ((*it) == NULL) {
			clog << L"item pulled out from list" << KIR4::eol;
			myList.erase(it);
			it = myList.begin();
		}
		else {
			it++;
		}
	}
}

//namespace StartUploader {
//	struct Flag {
//		std::string name, value;
//		Flag(const char *name, const char *value) :name(name), value(value) {
//
//		}
//		Flag(const Flag &flag) :name(flag.name), value(flag.value) {
//
//		}
//	};
//
//	std::string GenerateCommandList(__int32 id, __int32 flags, std::list<Flag> args_list) {
//		std::string args;
//
//		args += "ID'" + std::to_string(id) + "'FLAGS'" + std::to_string(flags) + '\'';
//
//		if (args_list.size()) {
//			for (auto &it : args_list) {
//				args += it.name + '\'' + it.value + '\'';
//			}
//		}
//
//		return args;
//	}
//
//	std::string GetArgList() {
//		std::string ret = "";
//
//		std::list<Flag> list;
//		__int32 id, flags;
//		char name[256], value[256];
//		std::string path;
//
//		do {
//			//clog << "ARGS name (* end): " >> name;
//			if (name[0] != '*') {
//				//clog << "ARGS value (* end): " >> value;
//				if (name[0] != '*') {
//					list.push_back(Flag(name, value));
//				}
//				else
//					break;
//			}
//			else
//				break;
//		} while (true);
//
//		ret = GenerateCommandList(id, flags, list);
//
//		return ret;
//	}
//
//	//C:\Own Program Files\MEGA\c++\Visual Studio\UYI\UYI\Release\UYI.exe
//	//	0
//	//	11
//	//	USERNAME
//	//	admin66
//	//	SERVER
//	//	127.0.0.1:4356
//	//	DIR
//	//	C : \Users\Kirtapsz\Desktop\MENTES
//}