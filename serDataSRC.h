#pragma once
/*

https://github.com/nlohmann/json

*/
#include "stdafx.h"
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

string getDocumentsPath() {
	string tmp = "";
	CHAR documents[MAX_PATH];
	HRESULT result = SHGetFolderPath(NULL, CSIDL_MYDOCUMENTS, NULL, SHGFP_TYPE_CURRENT, documents);
	tmp = documents;
	return tmp;
}

struct SerData {
	string PathLocalFolder = getDocumentsPath();
	string LoginUsername = "";
	string LoginPassword = "";
	string IDKEY = "";
};

SerData serdata;
json jsonData;

void Serialize() {
	jsonData.clear();
	jsonData["PathLocalFolder"] = serdata.PathLocalFolder;
	jsonData["LoginUsername"] = serdata.LoginUsername;
	jsonData["LoginPassword"] = serdata.LoginPassword;
	jsonData["IDKEY"] = serdata.IDKEY;
	ofstream out("data.json");
	out << jsonData;
	out.close();
}

void Deserialize() {
	ifstream in("data.json");
	if (in) {
		in >> jsonData;
	}
	else {
		return;	// so this is the first execution of the program
	}
	string tmp1 = jsonData.find("PathLocalFolder").value();
	serdata.PathLocalFolder = tmp1;
	string tmp2 = jsonData.find("LoginUsername").value();
	serdata.LoginUsername = tmp2;
	string tmp3 = jsonData.find("LoginPassword").value();
	serdata.LoginPassword = tmp3;
	string tmp4 = jsonData.find("IDKEY").value();
	serdata.IDKEY = tmp4;
	in.close();
}